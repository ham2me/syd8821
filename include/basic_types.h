/*
	Copyright (C) 2011 by PixArt
	ms_shia@pixart.com.tw
*/

#ifndef _BASIC_TYPES_H_
#define _BASIC_TYPES_H_

#include "ARMCM0.h"

#ifndef NULL
	#define NULL ((void *) 0)
#endif


#define START_TX_ENC 0x01
#define START_RX_ENC 0x02
	
#define BIT(s)				((uint8_t)1<<(s))
#define BIT0					0x01
#define BIT1					0x02
#define BIT2					0x04
#define BIT3					0x08
#define BIT4					0x10
#define BIT5					0x20
#define BIT6					0x40
#define BIT7					0x80
#define U16BIT(s)			((uint16_t)1<<(s))
#define U32BIT(s)			((uint32_t)1<<(s))



#define SETBITS(v,bits)		((v) |= (bits))
#define CLRBITS(v,bits)		((v) &= ~(bits))
#define CHKBITS(v,bits)		(((v) & (bits)) == (bits))

#define HIBYTE(int16_t)			((u8)((int16_t) >> 8))
#define LOBYTE(int16_t)			((u8)((int16_t) & 0xff))
#define HIWORD(s32)			((u16)((s32) >> 16))
#define LOWORD(s32)			((u16)((s32) & 0xffff))
#define MAKEWORD(hb,lb)		((u16)(hb) << 8 | (lb))
#define MAKEDWORD(hw,lw)	((u32)(hw) << 16 | (lw))

#define LowByte(u16)			((u8)(u16	 ))
#define HighByte(u16)		((u8)(u16 >> 8))
#define VAL16(x) 				(u8)((x) & 0xff), (u8)(((x) >> 8) & 0xff)

//#define REENTRANT			large reentrant 

#endif //_BASIC_TYPES_H_



