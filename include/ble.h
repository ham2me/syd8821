#ifndef _BLE_H_
#define _BLE_H_

#include "ARMCM0.h"
#include "config.h"
//#include "lib.h"
#include "ble_slave.h"

#define PPG_RAW_REPORT			0
#define HEART_RATE_REPORT 		1
#define BATTERY_LEVEL_REPORT    2
//#define BATTERY_LEVEL_REPORT	4

#define BLE_MAX_PACKET_SIZE 20
//#define LOW_POWER_ADVERTISING
#define LOW_POWER_ADV_INTV	600 //ms
#define GATT_SEND_DURATION 	15 //ms
#define GATT_SEND_DURATION_TICKS 	(GATT_SEND_DURATION * 32 + 30) // ticks

enum BLE_USER_COMMAND{
BLE_GET_TIME=0x89,
BLE_SET_TIME=0xc2,
BLE_SET_OCLOCK=0x83,
BLE_SYNC_USER_DATA=0xc4,
BLE_GET_CURRENT_STEP=0xc6,


};
/*
enum _MAC_INT_ST_{
	MAC_TX_INT				= 0x01,
	MAC_RX_INT				= 0x02,
	MAC_ADVEND_INT		= 0x04,
	MAC_CONNSET_INT		= 0x08,
	MAC_CONNLOST_INT		= 0x10,
	MAC_CONNEVTEND_INT	= 0x20,
	MAC_SLEEP_INT			= 0x40,
	MAC_WAKEUP_INT		= 0x80,

};
*/
enum _BLE_EVT_TYPE_{
	BLE_ADV_END                 = 0x01,
	BLE_CONNECTED               = 0x02,
	BLE_DISCONNECTED            = 0x04,
	BLE_PASSKEY_REQ             = 0x08,
	BLE_SHOW_PASSKEY_REQ        = 0x10,
};

typedef enum {
    SENSOR_HEARTRATE        = 0x01,
    SENSOR_PEDOMETER        = 0x02,
    SENSOR_ALL              = 0xFF,
}SENSOR_DATA_TYPE;

#if 0
enum _BLE_ADDRESS_TYPE_{
	PUBLIC_ADDRESS_TYPE	= 0x00,
	RANDOM_ADDRESS_TYPE 	= 0x01,
};


enum _ADV_CH_PKT_TYPE_{
	ADV_IND 			= 0x00,
	ADV_DIRECT_IND 	= 0x01,
	ADV_NOCONN_IND	= 0x02,
	SCAN_REQ			= 0x03,
	SCAN_RSP			= 0x04,
	CONNECT_REQ		= 0x05,
	ADV_SCAN_IND		= 0x06,
};
#endif

#ifdef _BLE_C
#else	

extern void Ble_Init(void);
extern void ble_task(void);
extern void ProcessBleTimer(void);
//extern bool Ble_send_HRD_data(uint8_t u8_hr);
extern void Ble_adv_start(uint8_t pairing_en);
extern bool Ble_RF_active_get(void);
extern void Ble_RF_active_set(bool active);
extern uint8_t  ble_gatt_data_notify(uint8_t *msg, uint16_t len);
extern bool BleConnected(void);
extern bool ProcessSensorData(SENSOR_DATA_TYPE DataType);
extern void GAP_Evt_Callback(struct gap_ble_evt *p_evt);
extern void HRD_Open(void);
extern void HRD_Close(void);
extern void ble_power_done(uint32_t wakeup_io);

#endif

typedef  void (*pR)(uint8_t, uint8_t *);
typedef  void (*pW)(uint8_t, uint8_t);

#define BBRFRead    (*(pR)0x000025dd)
#define BBRFWrite   (*(pW)0x000025ff)

extern uint8_t HR_Is_Open;

#endif

