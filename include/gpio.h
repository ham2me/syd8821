#ifndef _GPIO_H_
#define _GPIO_H_

#include "ARMCM0.h"
#include "gpio.h"
#include "config.h"

typedef enum {
    GPIO_BANK_0,  // GPIO  0 ~ 31
    GPIO_BANK_1,  // GPIO 32 ~ 38
    GPIO_BANK_NUM,
    GPIO_BANK_SIZE = 32,
} GPIO_BANK_TYPE;

typedef enum {
    LEVEL_TRIGGER,
    EDGE_TRIGGER,
} GPI_INT_TRIGGER_TYPE;

typedef enum {
    POL_RISING_HIGH,
    POL_FALLING_LOW,
} GPI_INT_POLARITY_TYPE;

typedef void (*GPI_IRQ_CB_TYPE)(GPIO_BANK_TYPE type, uint32_t bitmask);

void gpi_irq_set_cb(GPI_IRQ_CB_TYPE cb);
void gpi_config(uint32_t io, bool en, bool pull);
bool gpi_get(uint32_t io);
void gpi_enable_int(uint32_t io, GPI_INT_TRIGGER_TYPE trigger, GPI_INT_POLARITY_TYPE pol);
void gpi_disable_int(uint32_t io);

void gpo_config(uint32_t io, int val);
void gpo_toggle(uint32_t io);
void gpo_set(uint32_t io);
void gpo_clr(uint32_t io);

#endif //_GPIO_H_
