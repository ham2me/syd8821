#include "debug.h"
#include "delay.h"
#include "ble_slave.h"  // for sys_* functions
#include "pad_mux_ctrl.h"
#include "pwm_led.h"
#include "pwm.h"

#define LED0  14
#define LED1  15
#define LED2  16
#define KLIGHT 7


static __align(4) SEQ_COMMON_TYPE pwm_compare[2];

int main()
{
    
	__disable_irq();
	
	// UART0 as debug port
	pad_mux_write(20, 7);
	pad_mux_write(21, 7);

	sys_mcu_clock_set(MCU_CLOCK_64_MHZ);  // Set MCU Clock 64M
    sys_mcu_rc_calibration();  // RC bumping
    
	uint8_t c32k = sys_32k_lpo_calibration();

    dbg_init();
	dbg_printf("SYD8821 LED TEST c32k=%d\n", c32k);
	
	// LEDs
	pad_mux_write(LED0, 10);
	pwm_led_set_pwm(LED0%6, 50, 100);
	pwm_led_start(LED0%6);
	
	pad_mux_write(LED1, 10);
	pwm_led_set_pwm(LED1%6, 300, 1000);
	pwm_led_start(LED1%6);

	pad_mux_write(LED2, 10);
	pwm_led_set_pwm(LED2%6, 3000, 10000);
	pwm_led_start(LED2%6);
	
	pad_mux_write(KLIGHT, 8);
	pwm_stop();
	pwm_set_prescaler(2);
	pwm_set_decoder(PWM_COMMON);
	pwm_set_seq_mode(SINGLE_SEQUENCE_CONTINUE);
	pwm_compare[0].cmp_apply_all=10 | ACTIVE_LEAVE_POL;   //SEQ[0]
	pwm_compare[1].cmp_apply_all=10 | ACTIVE_LEAVE_POL;   //SEQ[1]
	pwm_set_seq(0, 2, &pwm_compare);
	pwm_set_period_length(1000);
	pwm_start(0);
	
	__enable_irq();

	while(1){
	    delay_ms(500);
        dbg_printf("t %d\n", SysTick->VAL);
	}
    return 0;
}
