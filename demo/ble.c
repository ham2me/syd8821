#include "config.h"
#include "gpio.h"
#include "debug.h"
#include "delay.h"
#include "config.h"
#include "ble_slave.h"
#include "ble_profile.h"
#include "string.h"
#include "pad_mux_ctrl.h"


enum CCCD_STATUS
{
	DISABLE = 0x00,
	ENABLE = 0x01
};

//wechat sport flag
#define WX_STEP_FLAG     0x01
#define WX_DISTANCE_FLAG 0x02
#define WX_CALORIE_FLAG  0x04

#define test_data 10000
uint32_t current_step 		= test_data;
uint32_t current_distance   = test_data * 0.6;
uint32_t current_calorie    = (0.6 * 65* test_data *78)/10000;	
uint32_t target_step = 15000;


static struct gap_att_report_handle *g_report;

//notify 标志 1代表已经使能 0代表未使能
static uint8_t start_tx=0;
uint8_t wechat_tx=0;
uint8_t battery_tx=0;
uint8_t wx_fea1_tx=0;
uint8_t wx_fea2_tx=0;


//connection
uint8_t  connect_flag=0;

uint8_t SCAN_DATA[]={
#ifdef UUID_Wechat
	0x03,// length
	0x03,// AD Type: Complete list of 16-bit UUIDs 
	UUID_Wechat&0xff,
	UUID_Wechat>>8,
#endif
};

static void ble_init(void);
uint8_t BLE_SendData(uint8_t *buf, uint8_t len);
uint8_t Battery_SendData(uint8_t *buf, uint8_t len);
uint8_t Wechat_SendData(uint16_t uuid, uint8_t *buf, uint8_t len);



static void setup_adv_data()
{
	struct gap_adv_params adv_params;	
	static struct gap_ble_addr dev_addr;
	
	adv_params.type = ADV_IND;
	//adv_params.channel = 0x07;    // advertising channel : 37 & 38 & 39
	
	adv_params.channel = 0x01;    // advertising channel : 37
	
	adv_params.interval = 0x640;  // advertising interval : unit 0.625ms)
	adv_params.timeout = 0x3FFF;    // timeout : uint seconds
	adv_params.hop_interval = 0x03;  //0x1c 
	adv_params.policy = 0x00;   

	gap_s_adv_parameters_set(&adv_params);

	gap_s_ble_address_get(&dev_addr);
	
	uint8_t data[32];
	uint8_t* p = data;
	
	*p++ = 2; // length
	*p++ = 1; // flags
	*p++ = 5; // LE Limited Discoverable Mode & BR/EDR Not Supported
	
	*p++ = 9;
	*p++ = 0xff;  // MANUFACTURER SPECIFIC DATA 
	*p++ = 0;
	*p++ = 0;
	for(int i=0; i<6; i++) *p++ = dev_addr.addr[5-i]; // MAC reversed
	
	int sz = _gatt_database_value[0] - 7;
	if(sz > 16) sz = 16;
	*p++ = sz + 1;
	*p++ = 9; // local name
	for(int i=0; i<sz; i++) *p++ = _gatt_database_value[7+i];

	gap_s_adv_data_set(data, p-data, SCAN_DATA, sizeof(SCAN_DATA)); 
}

/*
uint8_t target
0:fast
1:slow
*/
void BLSetConnectionUpdate(uint8_t target){
	struct gap_link_params  link_app;
	struct gap_smart_update_params smart_params;
	uint8_t buffer_cha1[5]={0XFC,0X01,0X00,0X00,0X00};
	gap_s_link_parameters_get(&link_app);
	dbg_printf("interval:%x latency:%x\r\n",link_app.interval,link_app.latency);
	switch(target){
		case 0: 
				if((link_app.latency !=0) && (link_app.interval >0x10)){
					/* connection parameters */
					smart_params.updateitv_target=0x0010;  //target connection interval (60 * 1.25ms = 75 ms)
					smart_params.updatesvto=0x00c8;  //supervisory timeout (400 * 10 ms = 4s)
					smart_params.updatelatency=0x0000;
					smart_params.updatectrl=SMART_CONTROL_LATENCY | SMART_CONTROL_UPDATE;
					smart_params.updateadj_num=MAX_UPDATE_ADJ_NUM;
					gap_s_smart_update_latency(&smart_params);
				}
				dbg_printf("SetUpdate ota link\r\n");
				BLE_SendData(buffer_cha1,5);
		break;
		case 1:
				if((link_app.latency <0x000A) && (link_app.interval <0x0050)){
					/* connection parameters */
					smart_params.updateitv_target=0x0050;
					smart_params.updatelatency=0x000A;
					smart_params.updatesvto=0x0258;
					smart_params.updatectrl=SMART_CONTROL_LATENCY | SMART_CONTROL_UPDATE;
					smart_params.updateadj_num=MAX_UPDATE_ADJ_NUM;
					gap_s_smart_update_latency(&smart_params);	   
					dbg_printf("SetUpdate ios link\r\n");
				}
		break;
	}
}


static void ble_gatt_read(struct gap_att_read_evt* pe)
{
    switch(pe->uuid){
        case UUID_WechatRead: {
		    uint8_t temp[20] = {0};
		    static struct gap_ble_addr dev_addr;
		    gap_s_ble_address_get(&dev_addr);
		    
		    /*get bluetooth address */
		    temp[0] = dev_addr.addr[5];
		    temp[1] = dev_addr.addr[4];
		    temp[2] = dev_addr.addr[3];
		    temp[3] = dev_addr.addr[2];
		    temp[4] = dev_addr.addr[1];
		    temp[5] = dev_addr.addr[0];
		    gap_s_gatt_read_rsp_set(6,temp);
		    return;
        }
        case UUID_BatteryLevel: {
		    uint8_t temp[2] = {100};
		    gap_s_gatt_read_rsp_set(1,temp);
		    return;
        }
        case UUID_WechatPedometerMeasurement: {
		    uint8_t gatt_buf[10]={0x00};
		    
		    gatt_buf[0] = WX_STEP_FLAG|WX_DISTANCE_FLAG|WX_CALORIE_FLAG;
		    
		    gatt_buf[1] = (uint8_t)(current_step);
		    gatt_buf[2] = (uint8_t)(current_step >> 8);
		    gatt_buf[3] = (uint8_t)(current_step >> 16);
		    
		    gatt_buf[4] = (uint8_t)(current_distance);
		    gatt_buf[5] = (uint8_t)(current_distance >> 8);
		    gatt_buf[6] = (uint8_t)(current_distance >> 16);
			    
		    gatt_buf[7] = (uint8_t)(current_calorie);
		    gatt_buf[8] = (uint8_t)(current_calorie >> 8);
		    gatt_buf[9] = (uint8_t)(current_calorie >> 16);
		    
		    gap_s_gatt_read_rsp_set(sizeof(gatt_buf), gatt_buf);
		    return;
        }
        case UUID_WechatSportTarget: {
		    uint8_t gatt_buf[4]={0x00};
		    
		    gatt_buf[0] = WX_STEP_FLAG;
		    gatt_buf[1] = (uint8_t)(target_step);
		    gatt_buf[2] = (uint8_t)(target_step >> 8);
		    gatt_buf[3] = (uint8_t)(target_step >> 16);

		    gap_s_gatt_read_rsp_set(sizeof(gatt_buf), gatt_buf);
		    return;
        }
    }

    ble_gatt_read_def(pe->uuid);
}

//接收函数
static void ble_gatt_write(struct gap_att_write_evt* pe)
{
	// rx data
	//evt.data是收取到的数据buf
	if(pe->uuid== UUID_UartWrite)
	{
	}
	else if(pe->uuid== UUID_WechatSportTarget)
	{
		if(pe->data[0] == WX_STEP_FLAG)
		{
			target_step = ((uint32_t)pe->data[3] << 16) | ((uint32_t)pe->data[2] << 8) | pe->data[1];
		}
		dbg_hexdump("set target step data\r\n",pe->data,pe->sz);
		dbg_printf("set target step:%d\r\n",target_step);
	}
}
//发送函数
uint8_t BLE_SendData(uint8_t *buf, uint8_t len)
{
	struct gap_att_report report;
	
	if(start_tx == 1)
	{
		report.primary = UUID_Uart;
		report.uuid = UUID_UartNotify;
		report.hdl = HANDLE_UartNotify;					
		report.value = BLE_GATT_NOTIFICATION;
		return gap_s_gatt_data_send(BLE_GATT_NOTIFICATION, &report, len, buf);
	}
	return 0;
}


uint8_t Wechat_SendData(uint16_t uuid, uint8_t *buf, uint8_t len)
{
	struct gap_att_report report;
	
	report.primary = UUID_Wechat;
	report.uuid = uuid;
	if(len > 20) len = 20;
	
	if((wechat_tx == ENABLE) & (UUID_WechatIndicate == uuid))
	{
		report.hdl = HANDLE_WechatIndicate;
		report.value = BLE_GATT_NOTIFICATION;
		return gap_s_gatt_data_send(BLE_GATT_NOTIFICATION, &report, len, buf);
	}
	else if((wx_fea1_tx == ENABLE) & (UUID_WechatPedometerMeasurement == uuid))
	{
		report.hdl = HANDLE_WechatPedometerMeasurement;
		report.value = BLE_GATT_NOTIFICATION;
		return gap_s_gatt_data_send(BLE_GATT_NOTIFICATION, &report, len, buf);		
	}
	else if((wx_fea2_tx == ENABLE) & (UUID_WechatSportTarget == uuid))
	{
		report.hdl = HANDLE_WechatSportTarget;	
		report.value = BLE_GATT_INDICATION;
		return gap_s_gatt_data_send(BLE_GATT_INDICATION, &report, len, buf);
	}
	else
	{
		return 0;
	}
}

uint8_t Battery_SendData(uint8_t *buf, uint8_t len)
{
	struct gap_att_report report;
	
	if(battery_tx == 1)
	{
		report.primary = UUID_BatteryService;
		report.uuid = UUID_BatteryLevel;
		report.hdl = HANDLE_BatteryLevel;					
		report.value = BLE_GATT_NOTIFICATION;
		return gap_s_gatt_data_send(BLE_GATT_NOTIFICATION, &report, len, buf);
	}
	return 0;
}

void ble_evt_callback(struct gap_ble_evt *pe)
{
    dbg_printf("ble_evt_callback %x\n", pe->evt_code);
    
	if(pe->evt_code == GAP_EVT_ADV_END)
	{		
		gap_s_adv_start();
		dbg_printf("GAP_EVT_ADV_END\r\n");
	}
	else if(pe->evt_code == GAP_EVT_CONNECTED)	 //连接事件
	{
		connect_flag=1;								 //连接状态
		start_tx = 0;
		wechat_tx=0;
		battery_tx=0;
		wx_fea1_tx=0;
		wx_fea2_tx=0;
		
		
		
		dbg_hexdump("GAP_EVT_CONNECTED addr:",pe->evt.bond_dev_evt.addr,sizeof(pe->evt.bond_dev_evt.addr));
		
		//BLSetConnectionUpdate(1);
	}
	else if(pe->evt_code == GAP_EVT_DISCONNECTED) //断连事件
	{	
		connect_flag=0;								 //连接状态
		start_tx = 0;
		wechat_tx=0;
		battery_tx=0;
		wx_fea1_tx=0;
		wx_fea2_tx=0;
        dbg_printf("GAP_EVT_DISCONNECTED(%02x)\r\n",pe->evt.disconn_evt.reason);
		gap_s_adv_start();
	}
	else if(pe->evt_code == GAP_EVT_ATT_HANDLE_CONFIGURE)
	{	
//		#ifdef CONFIG_UART_ENABLE 
//			dbg_printf("config_evt.hdl: %02x  \r\n",pe->evt.att_handle_config_evt.hdl);
//			dbg_printf("config_evt.value:%02x \r\n",pe->evt.att_handle_config_evt.value);
//			dbg_printf("config_evt.uuid:%02x  \r\n",pe->evt.att_handle_config_evt.uuid);
//		#endif
		
		if(pe->evt.att_handle_config_evt.uuid == UUID_Uart)
		{
			if(pe->evt.att_handle_config_evt.hdl == (HANDLE_UartNotify + 1))
			{
				if(pe->evt.att_handle_config_evt.value == BLE_GATT_NOTIFICATION)
				{
					dbg_printf("start_tx enable\r\n");
					start_tx = ENABLE;
				}
				else
				{			
					start_tx = DISABLE;
					dbg_printf("start_tx disable\r\n");
				}
			}
		}
		else if(pe->evt.att_handle_config_evt.uuid == UUID_Wechat)
		{
			if(pe->evt.att_handle_config_evt.hdl == (HANDLE_WechatIndicate + 1))
			{
				if(pe->evt.att_handle_config_evt.value == BLE_GATT_INDICATION)
				{
					dbg_printf("wechat_tx enable\r\n");
					wechat_tx = ENABLE;
				}
				else
				{	
					dbg_printf("wechat_tx disable\r\n");
					wechat_tx = DISABLE;
				}
			}
			else if(pe->evt.att_handle_config_evt.hdl == (HANDLE_WechatPedometerMeasurement + 1))
			{
				if(pe->evt.att_handle_config_evt.value == BLE_GATT_NOTIFICATION)
				{
					dbg_printf("wx_fea1_tx enable\r\n");
					wx_fea1_tx = ENABLE;
				}
				else
				{	
					dbg_printf("wx_fea1_tx disable\r\n");
					wx_fea1_tx = DISABLE;
				}
			}
			else if(pe->evt.att_handle_config_evt.hdl == (HANDLE_WechatSportTarget + 1))
			{
				if(pe->evt.att_handle_config_evt.value == BLE_GATT_INDICATION)
				{
					dbg_printf("wx_fea2_tx enable\r\n");
					wx_fea2_tx = ENABLE;
				}
				else
				{	
					dbg_printf("wx_fea2_tx disable\r\n");
					wx_fea2_tx = DISABLE;
				}
			}
		}
		else if(pe->evt.att_handle_config_evt.uuid == UUID_BatteryService)
		{
			if(pe->evt.att_handle_config_evt.hdl == (HANDLE_BatteryLevel + 1))
			{
				if(pe->evt.att_handle_config_evt.value == BLE_GATT_NOTIFICATION)
				{
					dbg_printf("battery_tx enable\r\n");
					battery_tx = ENABLE;
				}
				else
				{			
					battery_tx = DISABLE;
					dbg_printf("battery_tx disable\r\n");
				}
			}
		}
	}
	else if(pe->evt_code == GAP_EVT_ATT_WRITE)
	{
		ble_gatt_write(&pe->evt.att_write_evt);
	}
	else if(pe->evt_code == GAP_EVT_ATT_READ)
	{
		ble_gatt_read(&pe->evt.att_read_evt);
	}
	else if(pe->evt_code == GAP_EVT_ATT_HANDLE_CONFIRMATION)
	{
	}
	else if(pe->evt_code == GAP_EVT_ENC_KEY)
	{	
	}
	else if(pe->evt_code == GAP_EVT_ENC_START)
	{
	}
  else if(pe->evt_code == GAP_EVT_CONNECTION_UPDATE_RSP)
	{
	}
}


#define LEDR_PIN 15
#define LEDB_PIN 16
#define KEY_PIN  8

static void ble_init()
{	
    static struct gap_evt_callback GAP_Event;
	struct gap_wakeup_config pw_cfg;
	struct gap_profile_struct gatt;
	struct gap_pairing_req sec_params;
	struct gap_connection_param_rsp_pdu connection_params;

	gap_s_ble_init();
	
	//set profile
	gatt.report_handle_address = (uint32_t)_gatt_database_report_handle;
	gatt.primary_address	= (uint32_t)_gatt_database_primary;
	gatt.include_address	= (uint32_t)_gatt_database_include;
	gatt.characteristic_address	= (uint32_t)_gatt_database_characteristic;
	gatt.value_address = (uint32_t)_gatt_database_value;
	gap_s_gatt_profiles_set(&gatt);

	//set device bond configure
	sec_params.io = IO_NO_INPUT_OUTPUT;
	sec_params.oob = OOB_AUTH_NOT_PRESENT;
	sec_params.flags = AUTHREQ_BONDING;
	sec_params.mitm = 0;
	sec_params.max_enc_sz = 16;
	sec_params.init_key = 0;
	sec_params.rsp_key = (GAP_KEY_MASTER_IDEN |GAP_KEY_ADDR_INFO);
	gap_s_security_parameters_set(&sec_params);
 
	//set ble connect params
	connection_params.Interval_Min = 6;
	connection_params.Interval_Max = 9;
	connection_params.Latency = 100;
	connection_params.Timeout = 100;
	connection_params.PeferredPeriodicity = 6;
	connection_params.ReferenceConnEventCount = 50;
	connection_params.Offset[0] = 0;
	connection_params.Offset[1] = 1;
	connection_params.Offset[2] = 2;
	connection_params.Offset[3] = 3;
	connection_params.Offset[4] = 4;
	connection_params.Offset[5] = 5;
	gap_s_connection_param_set(&connection_params);

	//set connect event callback 
	GAP_Event.evt_mask=(GAP_EVT_CONNECTION_EVENT);
	GAP_Event.p_callback=&ble_evt_callback;
	gap_s_evt_handler_set(&GAP_Event);

	gap_s_gatt_report_handle_get(&g_report);

	bm_s_bond_manager_idx_set(0);
	
	setup_adv_data();

	//set MCU wakup source
	pw_cfg.timer_wakeup_en = 1;
	pw_cfg.gpi_wakeup_en = 1;
	pw_cfg.gpi_wakeup_cfg = U32BIT(KEY_PIN); 
	pw_cfg.gpi_wakeup_pol = U32BIT(KEY_PIN); 
	pmu_wakeup_config(&pw_cfg);
}



void sched_delay_31ms()
{
    if((SysTick->CTRL & 1) == 0)
        SysTick->CTRL = 1;

    unsigned v = SysTick->VAL;
    while(1){
        unsigned t = v - SysTick->VAL;
        if(t>>10)break;
        ble_sched_execute();
    }
}

static uint8_t keyst = 0;

int main()
{	
	__disable_irq();
	
	ble_init();
	
	sys_mcu_clock_set(MCU_CLOCK_64_MHZ);
    sys_mcu_rc_calibration();
	
	//!!! DONT use LPO 32768 for BLE. the connection is not stable 
	//sys_32k_clock_set(SYSTEM_32K_CLOCK_LPO);
	//delay_ms(10);
	//sys_32k_lpo_calibration();
	sys_32k_clock_set(SYSTEM_32K_CLOCK_XO);
	
	
	// UART0 as debug port
	pad_mux_write(20, 7);
	pad_mux_write(21, 7);
	
	// LED as running flag
    pad_mux_write(LEDB_PIN, 0);
    gpo_config(LEDB_PIN,1);

    pad_mux_write(LEDR_PIN, 0);
    gpo_config(LEDR_PIN,1);
	
	// KEY
    pad_mux_write(KEY_PIN, 0);
    gpi_config(KEY_PIN, true, true);
	
	dbg_init();
	dbg_printf("HAM2ME BLE demo (%s)\n", __DATE__);
	
	__enable_irq();	
	
	gap_s_adv_start();
	
	while(1)
	{				
        gpo_toggle(LEDB_PIN);
        gpo_clr(LEDR_PIN);
        
        if(!gpi_get(KEY_PIN)){
            if(keyst & 0x80){
                keyst += 1;
            } else {
                keyst = 0x80;
            }
        } else {
            keyst = (keyst + 1) & 0x3f;
            if(keyst == 1) dbg_printf("K\n");
        }
		
		//设备上报计步数据
		if(connect_flag==1 && keyst == 0x82){
			uint8_t gatt_buf[10]={0x00};
			
			dbg_printf("Report: step=%d distance=%d calorie=%d\n", current_step, current_distance, current_calorie);
			gatt_buf[0] = WX_STEP_FLAG|WX_DISTANCE_FLAG|WX_CALORIE_FLAG;
			
			gatt_buf[1] = (uint8_t)(current_step);
			gatt_buf[2] = (uint8_t)(current_step >> 8);
			gatt_buf[3] = (uint8_t)(current_step >> 16);
			
			gatt_buf[4] = (uint8_t)(current_distance);
			gatt_buf[5] = (uint8_t)(current_distance >> 8);
			gatt_buf[6] = (uint8_t)(current_distance >> 16);
				
			gatt_buf[7] = (uint8_t)(current_calorie);
			gatt_buf[8] = (uint8_t)(current_calorie >> 8);
			gatt_buf[9] = (uint8_t)(current_calorie >> 16);
			
			Wechat_SendData(UUID_WechatPedometerMeasurement, gatt_buf,sizeof(gatt_buf));
        
            gpo_set(LEDR_PIN);
		}
		
		//设备通知目标变化
		if(connect_flag==1 && keyst == 0x90){
			uint8_t gatt_buf[4]={0x00};
			
		    dbg_printf("Notify target step change:%d\r\n",target_step);
			
			gatt_buf[0] = WX_STEP_FLAG;
			gatt_buf[1] = (uint8_t)(target_step);
			gatt_buf[2] = (uint8_t)(target_step >> 8);
			gatt_buf[3] = (uint8_t)(target_step >> 16);
			
			Wechat_SendData(UUID_WechatSportTarget, gatt_buf,sizeof(gatt_buf));
            gpo_set(LEDR_PIN);
		}
		sched_delay_31ms();
		
		//PMU_CTRL->UART_EN = 0;
		//SystemSleep(POWER_SAVING_RC_OFF, FLASH_LDO_MODULE, 11000 , (PMU_WAKEUP_CONFIG_TYPE)(FSM_SLEEP_EN|TIMER_WAKE_EN|RTC_WAKE_EN));
	}	
}

