#!/usr/bin/python

import sys
sys.path.insert(0, '../tools')

from bleprofile import init, define, Profile, CharacteristicProperties

globals()['BatteryService.BatteryLevel.Notify'] = 1
init(globals())

uart = define(('Service', 1, 'test.uart'))
uart.add(define(('Characteristic', 2, 'test.uart_write', 'utf8s')), 'data', CharacteristicProperties.Write)
uart.add(define(('Characteristic', 3, 'test.uart_notify', 'utf8s')), 'data', CharacteristicProperties.Notify)

wechat = define(('Service', 0xfee7, 'test.wechat'))
wechat.add(define(('Characteristic', 0xfec7, 'test.wechat_write', 'utf8s')), 'data', CharacteristicProperties.Write)
wechat.add(define(('Characteristic', 0xfec8, 'test.wechat_indicate', 'utf8s')), 'data', CharacteristicProperties.Indicate|CharacteristicProperties.Read)
wechat.add(define(('Characteristic', 0xfec9, 'test.wechat_read', 'utf8s')), 'data', CharacteristicProperties.Read)
wechat.add(define(('Characteristic', 0xfea1, 'test.wechat_pedometer_measurement', 'utf8s')), 'data', CharacteristicProperties.Notify|CharacteristicProperties.Read)
wechat.add(define(('Characteristic', 0xfea2, 'test.wechat_sport_target', 'utf8s')), 'data', CharacteristicProperties.Indicate|CharacteristicProperties.Write|CharacteristicProperties.Read)

Profile(sys.stdout,
    GenericAccess('HAM2ME test', 'Mouse', PeripheralPreferredConnectionParameters(6, 6, 48, 100)),
    GenericAttribute( (0, 0) ),
    DeviceInformation('HAM2ME', 'BLE-MODEL-001', 'BLE-001', '1.0a', '1.0b', '1.0c', PnpId(2, 0x93a, 0x4254, 1)),
    BatteryService(100),
    Uart('\x00'*16, '\x00'*16),
    Wechat('\x00'*16, '\x00'*16, '\x00'*16, '\x00'*16, '\x00'*16),
)


