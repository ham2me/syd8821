CTOOLS=/usr/bin/arm-none-eabi-
CFLAGS=-mthumb -mcpu=cortex-m0 -Iinclude -O2 -c

ofiles=$(subst src/,lib/,$(patsubst %.c,%.o,$(wildcard src/*.c)))
hfiles=$(wildcard include/*.h)

all: lib/syd8821.lib lib/xflash.o lib/xram.o lib/xapp.o

lib/syd8821.lib: $(ofiles)
	$(CTOOLS)ar cr $@ $^

$(ofiles): lib/%.o: src/%.c $(hfiles)
	$(CTOOLS)gcc $(CFLAGS) $< -o $@

lib/xflash.o: src/startup.S
	$(CTOOLS)gcc $(CFLAGS) -D __NO_SYSTEM_INIT -D __STARTUP_COPY_ETEXT $^ -o $@

lib/xram.o: src/startup.S
	$(CTOOLS)gcc $(CFLAGS) -D __NO_SYSTEM_INIT $^ -o $@

lib/xapp.o: src/startup.S
	$(CTOOLS)gcc $(CFLAGS) -D __NO_SYSTEM_INIT $^ -o $@

clean:
	-rm lib/*.o lib/syd8821.lib
