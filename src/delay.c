#include "config.h"
#include "delay.h"

// use SYSTICK
static void delay_tick(unsigned nn)
{
    // enable 32768 Hz timer
    if((SysTick->CTRL & 1) == 0)
        SysTick->CTRL = 1;

    unsigned v = SysTick->VAL;
    while(nn > 0){
        if(SysTick->VAL != v){
            v = SysTick->VAL;
            nn -= 1;
        }
    }
}

void delay_ms(uint16_t n){
    delay_tick(n * 4096 / 125);
}


void delay_us(uint16_t n){
    if(n >= 50){
        delay_tick(n * 512 / 15625 + 1);
        return;
    }
    while(n > 0){
        delay_tick(0);
        delay_tick(0);
        n -= 1;
    }
}
