#define  _DEBUG_C

#include "debug.h"
#include "uart.h"
#include <stdarg.h>
#include <stdio.h>

#ifdef CONFIG_UART_ENABLE

#define MAX_FORMAT_BUFFER_SIZE	(128)
static uint8_t s_formatBuffer[MAX_FORMAT_BUFFER_SIZE];

void dbg_init(void)
{
	uart_0_init(UART_RTS_CTS_DISABLE, UART_BAUD_115200);
}

void dbg_printf(char *format,...)
{
	uint8_t iWriteNum = 0;	
	va_list  ap;
	
	if(!format)
		return;
	
	va_start(ap,format);

	iWriteNum = vsprintf((char *)s_formatBuffer,format,ap);

	va_end(ap);
	
	if(iWriteNum > MAX_FORMAT_BUFFER_SIZE)
		iWriteNum = MAX_FORMAT_BUFFER_SIZE;

	uart_write(0, s_formatBuffer, iWriteNum);
}

void dbg_printf_sel(uint8_t id, char *format, ...)
{
    uint8_t iWriteNum = 0;	
	va_list  ap;
	
	if(!format)
		return;
	
	va_start(ap,format);

	iWriteNum = vsprintf((char *)s_formatBuffer,format,ap);

	va_end(ap);
	
	if(iWriteNum > MAX_FORMAT_BUFFER_SIZE)
        iWriteNum = MAX_FORMAT_BUFFER_SIZE;
    
    uart_write(id, s_formatBuffer, iWriteNum);
}

void dbg_hexdump(char *title, uint8_t *buf, uint16_t sz)
{
	int i = 0;
	
	if (title)
		dbg_printf((title));

	for(i = 0; i < sz; i++) 
	{
  		if((i%8) == 0)
			dbg_printf("[%04x] ",(uint16_t)i);

		dbg_printf("%02x ",(uint16_t)buf[i]);

		if(((i+1)%8) == 0)
			dbg_printf("\r\n");

	 } 

	if((i%8) != 0)
		dbg_printf("\r\n");
}
#endif

