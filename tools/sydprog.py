#!/usr/bin/python3

import os, sys, time, serial, struct

DP_IDCODE = 0x0
DP_ABORT  = 0x0
DP_CTRL   = 0x4
DP_RESEND = 0x8
DP_SELECT = 0x8
DP_RDBUF  = 0xc

AP_CSW    = 0x100
AP_TAR    = 0x104
AP_DRW    = 0x10c
AP_IDR    = 0x1fc

CHIPAP_CTRL1 = 0x1a4


ARM_SYSTICK = 0xE000E010
ARM_CPUID   = 0xE000ED00
ARM_VTOR    = 0xE000ED08
ARM_AIRCR   = 0xE000ED0C
ARM_DHCSR   = 0xE000EDF0
ARM_DCRSR   = 0xE000EDF4
ARM_DCRDR   = 0xE000EDF8
ARM_DEMCR   = 0xE000EDFC

ARM_RAM   = 0x20000000
ARM_FLASH = 0x10000000
ARM_APP   = 0x10010000

def readhex(hexfile, intarray):
    lno = 0
    mem = []
    base = 0
    base0 = 0
    with open(hexfile, 'r') as f:
        for line in f:
            lno += 1
            line = line.strip()
            if len(line)<6 or line[0] != ':': continue
            if line[7:9] == '01': break
            if line[7:9] == '05': continue
            if line[7:9] == '04':
                base = int(line[9:13], 16) * 0x10000
                if base0 == 0: base0 = base
                if (base >> 24) != (base0 >> 24):
                    base = base0 + len(mem)
                    print('base->0x%x size->0x%x' % (base, len(mem)))
                continue
            if line[7:9] != '00':
                raise ValueError('unknown data type @ %s:%d' % (hexfile, lno))
            n = int(line[1:3], 16)
            addr = base - base0 + int(line[3:7], 16)
            if n + addr > len(mem): 
                mem.extend([255] * (n+addr-len(mem)))
            i = 9
            while n > 0:
                mem[addr] = int(line[i:i+2], 16)
                i += 2
                addr += 1
                n -= 1
        return base0, bytearray(mem)
        
        
def readprofile(hfile):
    ret = {}
    bn = None
    bs = None
    nn = '_gatt_database_'
    with open(hfile, 'r') as f:
        for line in f:
            line = line.strip()
            n = line.find('//')
            if n >= 0:
                line = line[:n].strip()
            #print bn, len(bs), line
            n = line.find(nn)
            if n > 0:
                n += len(nn)
                bn = line[n : n+4]
                bs = bytearray()
                continue
            if bn == None: continue
            if line == '};':
                ret[bn] = bs
                bn = None
                continue
                
            for t in line.replace(',', ' ').replace('\t', ' ').replace('0x', '').split(' '):
                if len(t) == 2: bs.append(int(t, 16))
    return ret
            

def ispi():
    if 'sydprog.nopi' in os.environ: return False
    with open('/proc/cpuinfo', 'r') as f:
        for line in f:
            ts = line.replace('\n', '').replace('\t', '').replace(' ', '').split(':')
            if len(ts) != 2: continue
            if ts[0] == 'Hardware' and ts[1] == 'BCM2835': return True
    return False


class SWD:
    def __init__(self, clk, dio):
        import ctypes
    
        path = os.path.dirname(os.path.realpath(__file__))
        ext = ctypes.cdll.LoadLibrary("%s/gpiopi.so" % path)
        ext.gpio_init()
        
        def init():
            ret = ext.swd_init(clk, dio)
            if ret != 1: raise ValueError("SWD_init return %d" % ret)
            ext.swd_idle()
            
        def pulse(pin):
            ext.gpio_mode(pin, 1) # output
            ext.gpio_clr(pin)
            time.sleep(0.02)
            ext.gpio_set(pin)
            time.sleep(0.02)
            
        def exit():
            ext.swd_exit(clk, dio)
        
        def setpage(cfg):
            if (cfg & 0x100) and (cfg & 0xf0) != self.apage:  # AP_*
                self.apage = cfg&0xf0
                ext.swd_write(DP_SELECT, cfg&0xf0)
        
        def write(cfg, v):
            setpage(cfg)
            ack = ext.swd_write(cfg, v)
            if ack == 1: return
            
            init()
            ack = ext.swd_write(cfg, v)
            if ack == 1: return
            raise ValueError('SWD_write ack %d' % ack)

        def read(cfg):
            setpage(cfg)
            v = ctypes.c_uint()
            ack = ext.swd_read(cfg, ctypes.byref(v))
            if ack == 1: return v.value
            
            init()
            ack = ext.swd_write(cfg, v)
            if ack == 1: return v.value
            raise ValueError('SWD_read ack %d' % ack)
        
        
        exit()  # release SWD GPIO at first
        self.idle = ext.swd_idle        
        self.exit = exit
        self.pulse = pulse
        self.read = read
        self.write = write
        self.init = init
        self.csw = 0
        self.apage = 1

    def setcsw(self, size, inc):
        csw =  0x23000002  # 32 bits
        if size == 1:
            csw |= self.csw & 0x10
        elif inc:
            csw |= 0x10
        if csw != self.csw:
            self.csw = csw
            self.write(AP_CSW, csw)
        
    def aread(self, addr, size, inc=True):
        self.setcsw(size, inc)
        self.write(AP_TAR, addr)
        self.read(AP_DRW) # discard first read
        dat = []
        for i in range(size):
            dat.append(self.read(AP_DRW))
        if size == 1: return dat[0]
        return dat
        
    def awrite(self, addr, dat, inc=True):
        self.write(AP_TAR, addr)
        if isinstance(dat, list):
            self.setcsw(len(dat), inc)
            for v in dat:
                self.write(AP_DRW, v)
        else:
            self.write(AP_DRW, dat)

PXT_CODE_JUMP_BASE = 0x20020000

CMD_PXT_FLASH_READ  = 0xfc0c
CMD_PXT_FLASH_WRITE = 0xfc0d
CMD_PXT_FLASH_ERASE = 0xfc0e
CMD_PXT_AMBA_READ   = 0xfc0f
CMD_PXT_AMBA_WRITE  = 0xfc10
CMD_PXT_CODE_JUMP   = 0xfc11

FW_TAG = 0
FW_MEM_CONFIG = 1
FW_RUN_MODE = 2   # value 0 -> pram mode, 1 -> cache mode
FW_CACHE_LINE_SIZE = 3
FW_HEADER_ADDRESS = 4
FW_CODE_HEADER_ADDRESS = 5
FW_PROTOCOL_SETTING_ADDRESS = 6
FW_REGISTER_SEETING_ADDRESS = 7
FW_GATT_PROFILE_ADDRESS = 8
FW_BOND_MANAGER_MASTER_ADDRESS = 9
FW_BOND_MANAGER_SLAVE_ADDRESS = 10
FW_PROFILE_PARAMETERS_ADDRESS = 11
FW_CODE_PRAM_0_ADDRESS = 12
FW_CODE_PRAM_1_ADDRESS = 13
FW_CODE_CACHE_0_ADDRESS = 14
FW_CODE_CACHE_1_ADDRESS = 15
FW_FFFF_0 = 16
FW_CODE_IDX = 17
FW_CODE_0_DESC = 18
FW_CODE_0_VERSION = 19
FW_CODE_0_SIZE = 20
FW_CODE_0_CHECKSUM = 21
FW_CODE_1_DESC = 22
FW_CODE_1_VERSION = 23
FW_CODE_1_SIZE = 24
FW_CODE_1_CHECKSUM = 25
FW_CODE_120K_SIZE = 26
FW_CODE_120K_CHECKSUM = 27


nowrite = False
class SYD8821:
    def __init__(self, clk, dio, rst, dev):
        self.swdok = False
        if clk > 0:
            self.swd = SWD(clk, dio)
        self.rst = rst
        self.ser = serial.Serial(dev, 115200, timeout=0.1)
    
    def initswd(self):
        for n in range(3):
            self.swd.init()
            if self.swd.read(DP_IDCODE) != 0xbb11477:
                continue

            self.swd.write(DP_ABORT, 30)
            self.swd.write(DP_CTRL, 0x50000000)
            self.swdok = True
            return
            
        raise ValueError('NO syd8821 chip on SWD')
        
    def initser(self):
        for i in range(5):
            self.ser.read(4)
            self.reset()
            tcnt = 0
            while tcnt < 3:
                self.ser.write(b'\x06')
                t = self.ser.read(1)
                if len(t) != 0 and t[0] == 6: break
                tcnt += 1
            time.sleep(0.1)
            if tcnt < 3: break
                
        if tcnt == 3:
            raise ValueError('NO syd8821 chip on serial')
            
        self.ser.timeout = 0.5
    
    def reset(self):
        if self.rst > 0:
            self.swd.pulse(self.rst)
        else:
            self.ser.dtr = True
            time.sleep(0.02)
            self.ser.dtr = False
            time.sleep(0.05)
        
    def halt(self):
        for i in range(3):
            try:
                self.swd.awrite(ARM_DHCSR, 0xA05F0003)  # enables debug & halt.
                if self.swd.aread(ARM_DHCSR, 1) & 2: break
            except:
                self.initswd()
        
    def rhalt(self):
        self.swd.write(CHIPAP_CTRL1, 8)   # Silicon Labs-specific AP register
        self.halt()
        self.swd.awrite(ARM_DEMCR, 0x1) # Reset Vector Catch.
        self.swd.awrite(ARM_AIRCR, 0xFA050004) # reset the core
        self.swd.write(CHIPAP_CTRL1, 0)

    def readreg(self, idx):
        self.swd.aread(ARM_DHCSR, 1)
        self.swd.awrite(ARM_DCRSR, idx)
        c = 0
        while c < 8:
            c += 1
            v = self.swd.aread(ARM_DHCSR, 1)
            if v & (1<<16):
                break
        if c == 64:
            raise ValueError('SYD8821 readreg timeout, DHCSR=0x%x' % v)
        return self.swd.aread(ARM_DCRDR, 1)
        
    def writereg(self, idx, v):
        self.swd.awrite(ARM_DCRDR, v)
        self.swd.awrite(ARM_DCRSR, idx | 0x10000)
        for i in range(8):
            if self.swd.aread(ARM_DHCSR, 1) & (1<<16): break
            
            
    def miniterm(self, mtype):
        from select import select
        import socket
        
        if mtype < 2:
            fi = sys.stdin
            print('--miniterm timestamp=%s' % mtype)
        else:
            fi = None
            fs = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            fs.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            fs.bind(('0.0.0.0', mtype))
            fs.listen(1)
            print('--miniterm vcom=%d' % mtype)
            mtype = 2
        
        t = time.time()
        while True:
            if fi == None:
                (fi, caddr) = fs.accept()
                self.ser.read(64)  # clear serial port buffer
                print('--connect from %s' % caddr[0])
                msg = ''
        
            rlist, _, _ = select([fi, self.ser], [], [], 1)
            if self.ser in rlist:
                b = self.ser.read(128)
                if len(b) == 0: continue
                
                if mtype == 1:
                    nt = time.time() 
                    print('[%s] ' % ((nt - t) * 1000), end='') 
                    t = nt
                
                if mtype >= 2:
                    fi.send(b)
                else:
                    sys.stdout.write(str(b, 'ascii', 'ignore'))
                    sys.stdout.flush()
                    
                #print '< ', len(b), map(lambda c : '%02x' % c, b)
                
            if fi in rlist:
                if mtype >= 2:
                    b = fi.recv(128)
                else:
                    b = bytes(sys.stdin.readline(), 'ascii')
                    
                if len(b) == 0:
                    fi = None
                    fo = None
                    print('--disconnect from %s' % caddr[0])
                    
                elif mtype>=2:
                    for ch in b:
                        if len(msg) == 0 and ch in ('\x06', '\x07'):
                            fi.send(ch)  # quick response
                            if ch == '\x06' and time.time() >= t + 2:
                                print('--reset')
                                self.reset()
                                self.ser.write('\x06')
                                self.ser.read(16)
                                t = time.time()
                            continue
                            
                        elif len(msg) > 0 or ch == '\x01':
                            msg += ch
                            if len(msg) > 4 and len(msg) == msg[3] + 4:
                                #print '> ', len(msg), map(lambda c : '%02x' % c, msg)
                                self.ser.write(msg)
                                msg = ''
                            continue
                            
                        else:
                            print('E ', len(b), map(lambda c : '%02x' % c, b))
                            break
                
                else:
                    #print '> ', len(b), map(lambda c : '%02x' % c, b)
                    self.ser.write(b)
                    
        return 0
    
        
    def read(self, addr, size, isram=True, progress=None):
        if isram:
            cmd = CMD_PXT_AMBA_READ
        else:
            cmd = CMD_PXT_FLASH_READ
        ret = bytearray()
        for off in range(0, size, 32):
            sz = min(size-off, 32)
            self.ser.write(struct.pack('<BHBIB', 1, cmd, 5, addr+off, sz))
            
            if progress:
                sys.stdout.write('%s %02d%% ...  \r' % (progress, off*100/size))
                sys.stdout.flush()
            
            for n in range(3):
                b = self.ser.read(sz+8)
                if len(b) > 2: break
            
            #print '< ', len(b), map(lambda c : '%02x' % c, b)
            
            if len(b) < 2 or b[0] != 4 or b[1] != 0xe:
                raise ValueError('read @(%x+%x) return "%s"' % (addr, off, map(lambda c : '%02x ' % c, b)))
                
            ret += b[8:]
        return ret

    def write(self, addr, dat, isram=True, progress=None):
        if isram:
            cmd = CMD_PXT_AMBA_WRITE
        else:
            cmd = CMD_PXT_FLASH_WRITE
        for off in range(0, len(dat), 32):
            sz = min(len(dat)-off, 32)
            self.ser.write(struct.pack('<BHBIB', 1, cmd, 5+sz, addr+off, sz))
            self.ser.write(dat[off:off+sz])
            if progress:
                sys.stdout.write('%s %02d%% ...  \r' % (progress, off*100/len(dat)))
                sys.stdout.flush()
                
            b = self.ser.read(7)
            if len(b) < 2 or b[0] != 4 or b[1] != 0xe:
                raise ValueError('write return "%s"' % map(lambda c : '%02x' % c, b))
        if progress:
            print('%s OK ...  ' % progress)
            
    def erase(self, addr, size):
        self.ser.write(struct.pack('<BHBIB', 1, CMD_PXT_FLASH_ERASE, 5, addr, size//4096))
        for n in range(3):
            b = self.ser.read(7)
            if len(b) > 2: break
        
    def watch(self):
        self.swd.awrite(ARM_DHCSR, 0xA05F0000)  # run
        for n in range(10):
            time.sleep(0.5)
            try:
                self.swd.awrite(ARM_DHCSR, 0xA05F0003)  # halt
                print('PC=%x SP=%x' % (self.readreg(15), self.readreg(13)))
                self.swd.awrite(ARM_DHCSR, 0xA05F0000) 
            except ValueError:
                self.initswd()
                    
    def step(self):
        ecnt = 0
        zcnt = 0
        stall = 0
        while True:
            time.sleep(0.1)
            try:
                csr = self.swd.aread(ARM_DHCSR, 1)
                pc = self.readreg(15)
                sp = self.readreg(13)
                c = self.swd.aread(pc, 1)
                if zcnt < 5 and (csr == 0 or pc == 0):
                    zcnt += 1
                    time.sleep(0.05)
                    self.initswd()
                    continue
                zcnt = 0
                print('C=%x PC=%x SP=%x PSR=%x DHCSR=%x' % (c, pc, sp, self.readreg(16), csr))
                if csr & (1<<24):
                    stall = 0
                else:
                    stall += 1
                    if stall > 2:
                        self.swd.awrite(ARM_DHCSR, 0xA05F0023)  # halt & SNAPSTALL
                        stall = 0
                
                if c == 0 or ((pc>>20) != 0 and (pc>>20) != (ARM_RAM>>20)):
                    ecnt += 1
                    # self.writereg(15, mem[1]&0xFFFFFFFE) # PC
                    if ecnt > 4: break
                else:
                    ecnt = 0
                    
            except ValueError:
                ecnt += 1
                self.initswd()
                if ecnt > 4: sys.exit(1)
                continue
                
            try:
                self.swd.awrite(ARM_DHCSR, 0xA05F000D)  # unhalt & step
            except ValueError:
                pass
        
    def exit(self):
        if self.swdok:
            self.swd.awrite(ARM_DHCSR, 0xA05F0000)
            print('exit debug %x' % self.swd.aread(ARM_DHCSR, 1))
            self.swd.write(DP_CTRL, 0) # disable AP
            self.swdok = False
        self.swd.exit()
    
    def testmem(self):
        if not self.swdok: self.initswd()
        
        #self.swd.awrite(ARM_RAM, [0x1234])
        print(map(hex, self.swd.aread(0x20008280, 16)))
        
        print('ARM_DHCSR=0x%x' % self.swd.aread(ARM_DHCSR, 1))
        print('ARM_DEMCR=0x%x' % self.swd.aread(ARM_DEMCR, 1))
        print('ARM_CPUID=0x%x' % self.swd.aread(ARM_CPUID, 1))
        print('ARM_SYSTICK=%s' % map(hex, self.swd.aread(ARM_SYSTICK, 4)))


    def runram(self, mem):
        #self.write(base, mem, progress='Uploading')
        #pc, = struct.unpack('<I', mem[4:8])
        #pc = (((pc&0xfffffffe) - PXT_CODE_JUMP_BASE - 4) & 0x7fffff) >> 1
        #elf.write(PXT_CODE_JUMP_BASE, bytearray([ (pc>>11)&0xff, 0xf0 | (pc>>19), (pc>>0)&0xff, 0xf8 | (pc>>8)&0x7])) # BL -> pc
        #b = self.read(PXT_CODE_JUMP_BASE, 10)
        #print 'J ', len(b), map(lambda c : '%02x' % ord(c), b)
        
        self.initswd()
        self.halt()
        
        imem = []
        for n in range(len(mem)/4):
            imem.append( (mem[n*4+0]<<0) + (mem[n*4+1]<<8) + (mem[n*4+2]<<16) + (mem[n*4+3]<<24) )
        
        ecnt = 0
        addr = 0
        while addr < len(imem):
            c = min(len(imem)-addr, 256)
            try:
                self.swd.awrite(ARM_RAM+addr*4, imem[addr:addr+c])
                addr += 256
            except ValueError:
                ecnt += 1
                self.initswd()
                self.halt()
                if ecnt > 50:
                    raise ValueError("write ARM_RAM timeout")

        if imem[1] != 0:
            self.writereg(15, imem[1]&0xFFFFFFFE) # PC
            self.writereg(13, imem[0]) # SP
            
        if (self.readreg(16) & (1<<24)) == 0:
            self.writereg(16, 1<<24) # PSP T-flag
        self.swd.awrite(ARM_DEMCR, 0) # disable Reset Vector Catch.        

    def runflash(self, mem, profile, isapp):
        self.initser()
        osetting = self.read(0, 4096, False)
        
        if len(mem)%0x10000:
            mem += bytearray([255]*(0x10000 - len(mem)%0x10000)) 
        checksum = sum(mem) & 0xffff

        sformat = '<H3B12I11sB32s5sIH32s5sIHIH'
        szformat = struct.calcsize(sformat)
        settings = struct.unpack(sformat, osetting[0:szformat])
        blchanged = not isapp and (settings[FW_CODE_0_SIZE] != len(mem) or settings[FW_CODE_0_CHECKSUM] != checksum)
        if settings[FW_TAG] != 0x093a or 'BL & APP' not in str(settings[FW_CODE_1_DESC]) or blchanged:
            if isapp: raise ValueError("BL is not flashed")
            if profile == None: raise ValueError("need profile parameter")
            settings = [0x093a, 0, 1, 3]
            settings.extend([0, 0x40, 0xc0, 0x180, 0x500, 0x1000, 0x2000, 0x3000, 0x4000, 0x4000, 0x4000, 0x4000])
            settings.extend([bytes([255]*11), 1, bytes('SYDPROG BL','ascii'), bytes('1.0', 'ascii'), len(mem), checksum, bytes('SYDPROG BL & APP','ascii'), bytes('1.0', 'ascii'), len(mem), checksum, 0, 0])

        offset = settings[FW_CODE_CACHE_1_ADDRESS]
        if isapp:
            offset += settings[FW_CODE_0_SIZE]
            checksum += settings[FW_CODE_0_CHECKSUM]
            settings[FW_CODE_1_SIZE] = settings[FW_CODE_0_SIZE] + len(mem)
            settings[FW_CODE_1_CHECKSUM] = checksum

        setting = bytearray(struct.pack(sformat, *settings))
        setting.extend([255] * (settings[FW_PROTOCOL_SETTING_ADDRESS] - szformat))
            
        prset = [
            '\x01\x12\x49\x74\x8e\xa7\xff\xd6\xbe\x89\x8e\x01\x01\x01\x00\x00\x64\x00\x00\x08\x3a\x09\x01\x00',
            '\x33\x01\x00\x00\x00\x00\x00\x00\x33\x01\x00\x00\x00\x00\x00\x00\x01\x00\x01\x00\x01\x01\x96\xc2',
            '\x98\xd8\x45\x39\xa1\xf4\xa0\x33\xeb\x2d\x81\x7d\x03\x77\xf2\x40\xa4\x63\xe5\xe6\xbc\xf8\x47\x42',
            '\x2c\xe1\xf2\xd1\x17\x6b\xf5\x51\xbf\x37\x68\x40\xb6\xcb\xce\x5e\x31\x6b\x57\x33\xce\x2b\x16\x9e',
            '\x0f\x7c\x4a\xeb\xe7\x8e\x9b\x7f\x1a\xfe\xe2\x42\xe3\x4f\x49\x76\x96\x4f\xba\x36\xb6\xf8\x13\xc3',
            '\x53\x4e\x70\x59\x18\xb9\xf8\xce\xe3\x40\x61\x3d\x03\x38\xc1\x85\x22\x5d\x58\x25\x42\x98\x1e\x40',
            '\x9b\xed\x4f\x1a\xdc\x1a\x98\xa8\xae\xfa\x38\xdd\x69\x7b\xb9\x96\x43\x7c\x28\x45\xa5\x67\xa7\x19',
            '\xfd\x46\x4f\x14\x34\x97\x00\x00\x00\x0a\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff',
            '\x00\x00\x00\x00\x2e\x00\x37\x7f\x00\x37\x00\x06\x37\x01\x30\x37\x04\x00\x37\x10\xc0\x37\x13\x80',
            '\x37\x16\x14\x37\x11\xd8\x37\x3a\x1d\x37\x30\x00\x37\x37\x00\x37\x3e\x04\x37\x7f\x01\x37\x00\x40',
            '\x37\x18\x41\x37\x30\x03\x37\x50\x04\x37\x53\x06\x37\x55\x00\x37\x62\x27\x37\x51\x00\x37\x57\x7f',
            '\x37\x60\x0a\x37\x62\x27\x37\x63\x1e\x37\x66\x89\x37\x6d\x1f\x37\x7f\x02\x37\x08\x2c\x37\x38\xff',
            '\x37\x39\x17\x1c\x12\x0f\x1c\x13\x01\x1c\x16\x99\x1c\x14\x74\x1c\xd0\x84\x1b\x0b\x4a\x1b\x0c\x38',
            '\x1b\x0e\x01\x1b\x14\x03\x1b\x70\x20\x1b\x71\x62\x1b\x73\x5e\x1b\x1d\x3c\x1b\x1e\x01\x06\x78\x04']
        for v in prset:
            setting.extend(map(ord, v))
            
            
        setting.extend([255] * (settings[FW_GATT_PROFILE_ADDRESS] - len(setting)))
        if profile == None:
            setting.extend(osetting[ len(setting) : ])
        
        else:
            args = [ len(setting) + 20 ]
            args.append( args[-1] + len(profile['repo']) )
            args.append( args[-1] + len(profile['prim']) )
            args.append( args[-1] + len(profile['incl']) )
            args.append( args[-1] + len(profile['char']) )
            setting.extend(struct.pack('<IIIII', *args))
            setting.extend(profile['repo'])
            setting.extend(profile['prim'])
            setting.extend(profile['incl'])
            setting.extend(profile['char'])
            setting.extend(profile['valu'])
            setting.extend( [255] * (4096 - len(setting)) )
        
        global nowrite
        if not nowrite:
            if osetting[0] != 255:
                self.erase(offset, len(mem))
            self.write(offset, mem, False, 'write data')

            if setting != osetting:
                if osetting[0] != 255:
                    self.erase(0, len(setting))
                self.write(0, setting, False, 'write header')
        else:
            with open('setting_sydprog.bin', 'wb') as f:
                f.write(setting)
            

    def runhex(self, hexfile, profile):
        base, mem = readhex(hexfile, True)
        if base == ARM_RAM:
            return self.runram(mem)
        if base == ARM_FLASH:
            return self.runflash(mem, profile, False)
        if base == ARM_APP:
            return self.runflash(mem, profile, True)
        raise ValueError('base %x is not RAM' % base)


def run(syd):
    profile = None
    for cmd in sys.argv[1:]:
        if cmd[0] == '-':
            if cmd in ('-?', '-h', '-help'):
                print('''Usage: sydprog.py cmd1 cmd2 ...
    Supported cmds include:
        -reset | -r      reset chip
        -step  | -s      program step debug. need SWD support
        -watch | -w      program snapshot per second. need SWD support
        -term  | -t      miniterm without timestamp
        -terms | -ts     miniterm with timestamp
        -vcom            virtual COM server
        -testmem         show memory info. need SWD support
        <file>.hex       upload program
        <file>.h         set BLE profile
    ''')

            elif cmd in ('-r', '-reset'):
                syd.reset()
                
            elif cmd in ('-s', '-step'):
                syd.halt()
                syd.step()

            elif cmd in ('-w', '-watch'):
                syd.halt()
                syd.watch()
                
            elif cmd in ('-t', '-term'):
                syd.miniterm(0)

            elif cmd in ('-ts', '-terms'):
                syd.miniterm(1)

            elif cmd in ('-vcom'):
                syd.miniterm(3333)
                
            elif cmd == '-testmem':
                syd.testmem()

            else:
                print('E: invalid cmd %s' % cmd)
                break
                
        elif cmd.endswith('.h'):
            profile = readprofile(cmd)
            
        elif cmd.endswith('.hex'):
            syd.runhex(cmd, profile)
         
        else:
            print('E: invalid argument %s' % cmd)
            break


if __name__ == '__main__':
    if ispi():
        args = [2, 3, 4, '/dev/serial0']
    else:
        args = [0, 0, 0, '/dev/ttyUSB0']
        
    for n in range(len(args)):
        nm = ('clk', 'dio', 'rst', 'port')[n]
        v = os.environ.get('sydprog.' + nm, str(args[n]))
        if isinstance(args[n], int): v = int(v)
        args[n] = v
        
    nowrite = 'sydprog.nowrite' in os.environ

    syd = SYD8821(*args)
    try:
        run(syd)
    finally:
        syd.exit()

