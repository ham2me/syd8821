#!/usr/bin/python3

import os, sys, time, struct, sydprog

def readrom():    
    try:
        with open('rom.bin', 'rb') as f:
            b = f.read()
    except:
        syd = sydprog.SYD8821(2, 3, 4, '/dev/serial0')
        syd.initser()
        b = syd.read(0, 0xc000, progress='reading rom')
        with open('rom.bin', 'wb') as f:
            f.write(b)
    return bytearray(b)

def readsym(lib):
    define = {}
    
    def add(s):
        i = s.index(' ')
        define[ s[0:i] ] = s[i+1:]
    
    with open(lib, 'rb') as f:
        b = f.read()
        n = b.index(bytes('START_TX_ENC', 'ascii'))
        while True:
            k = b.index(0, n)
            add(str(b[n:k], 'ascii'))
            if b[k+1] != 1: break
            k += 2
            if b[k] & 0x80: k += 1
            n = k + 1
            
    return define
    
def toasm(f, b, define):
    VECTORS = (
        '__stack',
        'Reset_Handler',
        'NMI_Handler',
        'HardFault_Handler',
        '_vector_4',
        '_vector_5',
        '_vector_6',
        '_vector_7',
        '_vector_8',
        '_vector_9',
        '_vector_10',
        'SVC_Handler',
        '_vector_12',
        '_vector_13',
        'PendSV_Handler',
        'SysTick_Handler',
        'GPIO_IRQHandler',
        'LLC_IRQHandler',
        'SPIM_IRQHandler',
        'SPIS_IRQHandler',
        'I2CM0_IRQHandler',
        'I2CM1_IRQHandler',
        'UART0_IRQHandler',
        'UART2_IRQHandler',
        'I2CS_IRQHandler',
        'TIMER0_IRQHandler',
        'TIMER1_IRQHandler',
        'TIMER2_IRQHandler',
        'TIMER3_IRQHandler',
        'TIMER4_IRQHandler',
        'WDT_IRQHandler',
        'SW_IRQHandler',
        'PWM1_IRQHandler',
        'PWM2_IRQHandler',
        'PWM3_IRQHandler',
        'M2M_IRQHandler',
        'RTC_IRQHandler',
        'ISO_7816_IRQHandler',
        'IR_IRQHandler',
        'TRNG_IRQHandler',
        'GPADC_IRQHandler',
        'UART1_IRQHandler',
        'CAPDET_IRQHandler',
        'PDM_IRQHandler',
        'LLC2_IRQHandler',
        'LLC3_IRQHandler',
        'I2S_IRQHandler',
        'INTCOMB_IRQHandler',)
    
    sym = {}
    sym[1] = '__vectors'
    data = struct.unpack('<%dI' % (len(b)/4), b)
    for n in range(len(VECTORS)):
        v = data[n]
        if v == 0: continue
        sym[v] = VECTORS[n]
        
    sym[0xacc0+1] = 'memcpy'
    sym[0xace4+1] = 'memset'
    sym[0xacf2+1] = 'memclr'
    sym[0xacf6+1] = 'memcpy2'
    sym[0xad08+1] = 'memcmp'
    sym[0xad22+1] = 'readi_le'
    sym[0xad36+1] = 'writei_le'
    sym[0xad48+1] = 'func000'
    sym[0xadc4+1] = 'func001'
    sym[0xaf98+1] = 'func002'
    sym[0xb066+1] = 'func003'
    sym[0xb0f6+1] = 'func004'
    
    sym[0x34c0+1] = 'cb_init'
    sym[0x34dc+1] = 'cb_is_init'
    sym[0x34fc+1] = 'cb_get_size'
    
    sym[0x18c2+1] = 'flash_read_pram'
    sym[0x18ec+1] = 'flash_000'

    sym[0x5c58+1] = '_return_popr4'
    sym[0x2df0+1] = '_mcu_clock_set'
    sym[0x2f7e+1] = '_mcu_clock_div_set'
    sym[0x2ce4+1] = '_sys_32k_clock_set'
    sym[0x2d68+1] = '_mcu_rc_calibration_0'
    sym[0x8568+1] = '_sys_32k_lpo_calibration'
    sym[0x85d8+1] = '_mcu_rc_calibration'
    

    sym[0x790+1] = '_flash_hdr_0'
    sym[0x966+1] = '_flash_hdr_1'
    sym[0x87a+1] = '_flash_hdr_2'
    
        
    f.write('/*\n')
    for k in sorted(define.keys()):
        v = define[k]
        f.write('#define %s %s\n' % (k, v))
        i = v.find('0x00')
        if i < 0: continue
        sym[ int(v[i+4:i+10], 16) ] = k
        
    f.write('''*/
    .syntax    unified
    .arch    armv6-m

    .section .text
    .thumb
    .thumb_func
    .align 2
''')

    off = 1   # last bit of PC is thumb type
    for v in data:
        if off in sym:
            s = sym[off]
            f.write('    .global %s\n.type	%s, %%function\n%s:\n' % (s, s, s))
            
        if off > 1 and off < len(VECTORS)*4 and v != 0:
            f.write('    .long %s\n' % sym[v])
        elif off+2 in sym:
            f.write('    .short %d\n' % (v&0xffff))
            s = sym[off+2]
            f.write('    .global %s\n.type	%s, %%function\n%s:\n' % (s, s, s))
            f.write('    .short %d\n' % (v>>16))
        else:
            f.write('    .long %d\n' % v)
            
        off += 4
    
    

b = readrom()
with open('rom.s', 'w') as f:
    toasm(f, b, readsym('../lib/syd8821_ble_slave.lib'))
os.system('/usr/bin/arm-none-eabi-as -mthumb -mcpu=cortex-m0 -o rom.o rom.s')
os.system('/usr/bin/arm-none-eabi-ld -Ttext 0 --entry Reset_Handler -o rom.elf rom.o')
os.system('/usr/bin/arm-none-eabi-objdump -Mforce-thumb -D rom.elf >rom.asm')


# base_intr_handle @0   flash_intr_handle@10000000  rom_intr_handle@00100000
# if (@2002aa08 == 1) call flash_intr_handle else call rom_intr_handle



