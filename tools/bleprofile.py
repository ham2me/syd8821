#!/usr/bin/python

import os, sys

ROOT = os.path.dirname(os.path.realpath(__file__))
namespace = None


class CharacteristicProperties:
    Broadcast=1
    Read=2
    WriteWithoutResponse=4
    Write=8
    Notify=16
    Indicate=32
    SignedWrite=64
    keys = ('Broadcast', 'Read', 'WriteWithoutResponse', 'Write', 'Notify', 'Indicate', 'SignedWrite')

class DefineParam:
    def __init__(self, stype, name = '*', prop = None, require=0):
        if stype == None:
            raise ValueError('stype None')
    
        self.stype = stype
        self.name = name
        self.require = require
        
        if isinstance(prop, int):
            v = []
            for ck in CharacteristicProperties.keys:
                cv = CharacteristicProperties.__dict__[ck]
                if prop & cv: cv = 1
                else: cv = 0
                v.append(cv)
            prop = v
            
        self.prop = prop
        
    def __repr__(self):
        if isinstance(self.stype, Define):
            st = self.stype.name
        elif isinstance(self.stype, dict):
            st = 'enum[%s]' % ','.join(self.stype.keys())
        else:
            st = self.stype
        st += ' ' + self.name.replace(' ', '_').lower()
        if not self.require: st += '=0'
        return st

class Define(object):
    def __init__(self, uri, getf):
        def setnm():
            self.uri = uri
            nm = ''
            for n in uri.split('.')[-1].split('_'):
                nm += n[0].upper() + n[1:]
            self.name = nm
    
        if isinstance(uri, str):
            setnm()
            ts = uri.split('.')
            self.initxml('%s/%s%ss/%s.xml' % (ROOT, ts[2][0].upper(), ts[2][1:], uri), getf)
        else:
            self.params = []
            for u in uri[3:]:
                self.add(u)
            self.type, self.uuid, uri = uri[0:3]
            setnm()


    def __repr__(self):
        plist = []
        for p in self.params:
            plist.append(repr(p))
            
        return '/*%s*/ %s(%s)' % (self.uri, self.name, ', '.join(plist))

    def initxml(self, fname, getf):
        def getflag(node, name):
            b = 0
            for t in node.getElementsByTagName(name):
                v = t.firstChild.nodeValue.lower()
                if v in ('mandatory', 'yes'): b = 1
                elif v in ('excluded', 'optional'): b = 0
                else: b = v
            return b
        
        def getparams(node, nm):
            ret = []
            for c in node.getElementsByTagName(nm):
                dt = None
                for t in c.getElementsByTagName('Format'):
                    dt = str(t.firstChild.nodeValue.lower())

                if dt in ('uint8', 'uint16', '16bit'):
                    evs = {}
                    for t in c.getElementsByTagName('Enumeration'):
                        key = str(t.getAttribute('key'))
                        if key != '':
                            evs[ str(t.getAttribute('value')) ] = int(key)
                            
                    if len(evs) > 1:
                        evs['_'] = dt
                        dt = evs
                    
                if dt == None:
                    dt = getf(str(c.getAttribute('type')))
                if dt == None:
                    for t in c.getElementsByTagName('Reference'):
                        dt = str(t.firstChild.nodeValue.lower().strip())
                        dt = getf(dt)
                if dt == None:
                    continue  # invalid xml

                ename = None
                if isinstance(dt, Define):
                    ename = '%s.%s.' % (self.name, dt.name)
                                        
                dt = DefineParam(dt, str(c.getAttribute('name')), None, getflag(c, 'Requirement'))
                    
                for t in c.getElementsByTagName('Properties'):
                    v = []
                    for ck in CharacteristicProperties.keys:
                        global namespace
                        if ename != None and (ename + ck) in namespace:
                            v.append(namespace[ename + ck])  # let env override xml setting
                        else:
                            v.append(getflag(t, ck))
                    dt.prop = v
                    break
                
                ret.append(dt)
            return ret
        
        from xml.dom.minidom import parse
        dom = parse(fname).documentElement
        self.type = dom.tagName
        self.uuid = int(dom.getAttribute('uuid'), 16)
        self.params = []
        
        params = []
        for c in dom.getElementsByTagName('Characteristics'):
            params = getparams(c, 'Characteristic')
        for c in dom.getElementsByTagName('Value'):
            params = getparams(c, 'Field')
        
        for p in params:
            if p.require == 1: self.params.append(p)
        for p in params:
            if p.require != 1: self.params.append(p)
    
    def add(self, stype, name = '*', prop = None, require=0):
        if isinstance(stype, DefineParam):
            self.params.append(stype)
        else:
            self.params.append(DefineParam(stype, name, prop, require))
        
    def build(self, profile, args, path):
        h = 0
        vh = 0
        if ('Service', 'Characteristic', 'Descriptor').index(self.type) != len(path):
            raise ValueError('%s\narg[%d](%s %s): in wrong level' % (self, n, type(arg), arg))

        h = profile.halloc()
        if len(path) == 1: vh = profile.halloc()
        if len(path) == 2: vh = h
            
        value = []
        for n in range(len(args)):
            arg = args[n]
            param = None
            if not isinstance(arg, tuple) or not isinstance(arg[0], Define):
                # match by index
                if n >= len(self.params):
                    raise ValueError('%s\narg[%d](%s %s): too many args' % (self, n, type(arg), arg))
                param = self.params[n]
                if isinstance(param.stype, Define):
                    if isinstance(arg, tuple):
                        arg = (param.stype, ) + arg
                    else:
                        arg = (param.stype, arg)
                    # fall through
                else:
                    comp = False
                    if isinstance(arg, int):
                        comp = isinstance(param.stype, dict) or param.stype in ('uint8', 'uint16', 'uint32', '16bit')
                    elif isinstance(arg, str):
                        comp = (isinstance(param.stype, dict) and arg in param.stype) or param.stype in ('utf8s', 'utf8')
                    if comp:
                        value.append( (param.stype, arg) )
                        continue 
                    else:
                        param = None # incompatible
                        
            else:
                # match by name
                for i in range(len(self.params)):
                    p = self.params[i]
                    if isinstance(p.stype, Define) and arg[0].name == p.stype.name:
                        param = p
                        break

            if arg[0].name == 'ClientCharacteristicConfiguration':
                pass
            elif param == None or arg[0].name != param.stype.name:
                raise ValueError('%s\narg[%d](%s %s): incompatible' % (self, n, type(arg), arg))
            
            npath = path + ( (self, h, vh, n), )
            arg[0].build(profile, arg[1:], npath)
            
            # Notify or Indicate, add ClientCharacteristicConfiguration automatically
            if param.prop != None and (param.prop[4] or param.prop[5]):
                ccc = Define.get('org.bluetooth.descriptor.gatt.client_characteristic_configuration')
                ch = profile.halloc()
                ccc.build(profile, [0], npath + ( (arg[0], ch, ch, -1), ) )
        
        npath = path + ((self, h, vh, -1),)
        profile.add(npath)
        if len(value) > 0: profile.addvalue(npath, value)
        
    
    cache = {}
    @staticmethod
    def get(uri):
        if uri == '': return None
        c = Define.cache.get(uri)
        if c == None:
            c = Define(uri, Define.get)
            Define.cache[uri] = c
        return c


def uwhex(n):
    return '0x%02x, 0x%02x' % (n & 0xff, (n>>8)&0xff)

class Profile(object):
    def __init__(self, f, *svcs):
        self.h = 0
        
        self.rep = []
        self.svc = []
        self.cha = []
        self.val = {}
        self.set = []
        self.rdf = {}
        
        for svc in svcs:
            svc[0].build(self, svc[1:], tuple())
         
        self.output(f)
    
    def halloc(self):
        self.h += 1
        return self.h
        
    def add(self, path):
        if len(path) == 1:
            self.set.append('UUID_%s 0x%04x' % (path[0][0].name, path[0][0].uuid))
            self.set.append('')
            
            self.svc.append('%s, 0x00, 0x28, %s, %s, // Service(%d-%d) %s' % (uwhex(path[0][1]), uwhex(self.h), uwhex(path[0][0].uuid), path[0][1], self.h, path[0][0].name))
            
        if len(path) == 2:
            self.set.append('UUID_%s 0x%04x' % (path[1][0].name, path[1][0].uuid))
            self.set.append('HANDLE_%s 0x%04x' % (path[1][0].name, path[1][2]))
            
            props = path[0][0].params[ path[0][3] ].prop
            prop = 0
            sprop = []
            for n in range(len(props)):
                if not isinstance(props[n], int):
                    continue
                prop |= props[n] << n
                if props[n]: sprop.append(CharacteristicProperties.keys[n])
            self.cha.append('%s, 0x03, 0x28, 0x%02x, %s, %s, // Characteristic(%d, %s, %s)' % (uwhex(path[1][1]), prop, uwhex(path[1][2]), uwhex(path[1][0].uuid), path[1][1], path[1][0].name, '|'.join(sprop)))
            
        
        
    def addvalue(self, path, value):
        if len(path) == 3 and path[2][0].name == 'ClientCharacteristicConfiguration':
            self.set.append('IDX_%s %d' % (path[1][0].name, len(self.rep)))
            self.rep.append('%s, %s, %s, %s, %s, // %s.%s' % (
                uwhex(path[0][0].uuid), uwhex(path[1][0].uuid), uwhex(path[1][2]), uwhex(path[2][1]), uwhex(value[0][1]), path[0][0].name, path[1][0].name))
        
        b = bytearray([0, 0, path[-1][2]&0xff, path[-1][2]>>8, 0x10, path[-1][0].uuid&0xff, path[-1][0].uuid>>8])
        for t,v in value:
            if isinstance(t, dict):
                if v in t: v = t[v]
                t = t['_']
            if t == 'uint8' or t == '8bit': b.append(v & 0xff)
            elif t == 'uint16' or t == '16bit': b.extend([v&0xff, v>>8])
            elif t == 'utf8s': b.extend(bytearray(v))
            else:
                raise ValueError('addvalue %s %s' % (t, v))
        b[0] = len(b) & 0xff
        b[1] = len(b) >> 8
        self.val[path[-1][2]] = '%s, // %s "%s"' % (', '.join(map(lambda x: '0x%02x' % x, b)), path[-1][0].name, value)
        
        if len(path) == 2:
            self.rdf[ path[-1][0].name ] = path[-1][2]
        
    
    def output(self, f):
        print >>f, '// generated by bleprofile.py'
        print >>f, '#ifndef ble_profile_h'
        print >>f, '#define ble_profile_h\n'
        
        for s in self.set:
            if s == '':
                print >>f, s
            else:
                print >>f, '#define %s' % s
            
        print >>f, '\nstatic const uint8_t _gatt_database_report_handle[] =\n{'
        print >>f, '   ', '%d,' % len(self.rep)
        for s in self.rep:
            print >>f, '   ', s
            
        print >>f, '};\n\nstatic const uint8_t _gatt_database_primary[] =\n{'
        for s in self.svc:
            print >>f, '   ', '0x09,', s
            
        print >>f, '};\n\nstatic const uint8_t _gatt_database_include[] =\n{'
        print >>f, '    0x0A, 0xFF, 0xFF, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00,'
            
        print >>f, '};\n\nstatic const uint8_t _gatt_database_characteristic[] =\n{'
        for s in self.cha:
            print >>f, '   ', '0x0a,', s
        print >>f, '   ', '0x0a, 0xff, 0xff, 0x00, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00, // End'
        
        print >>f, '};\n\nstatic const uint8_t _gatt_database_value[] =\n{'
        pos = {}
        cpos = 0
        for k in sorted(self.val.keys()):
            ts = self.val[k].replace('0x', '').split(', ')
            sz = int(ts[0], 16) + int(ts[1], 16) * 256
            pos[k] = (cpos, sz)
            cpos += sz
            print >>f, '   ', self.val[k]
        print >>f, '   ', '0xff, 0xff, 0xff, 0xff // End\n};\n'
        
        print >>f, '#define ble_gatt_read_def(uuid) switch(uuid) {\\'
        for k,v in self.rdf.items():
            cpos, sz = pos[v]
            print >>f, '    case UUID_%s: { gap_s_gatt_read_rsp_set(%d, (uint8_t*)&_gatt_database_value[%d]); break; } \\' % (k, sz-7, cpos+7)
        print >>f, '}'
        
        print >>f, '\n#endif'
        
def define(uri):
    global namespace
    df = Define.get(uri)
    def afunc(*args):
        return (df, ) + args
    namespace[df.name] = afunc
    return df

def definedir(path, show):
    for n in os.listdir(path):
        ts = n.split('.')[:-1]
        df = define('.'.join(ts))
        if show: print df

def init(dt = globals()):
    global namespace
    namespace = dt
    definedir(ROOT + '/Services', '-s' in sys.argv)
    definedir(ROOT + '/Characteristics', '-c' in sys.argv)
    definedir(ROOT + '/Descriptors', '-d' in sys.argv)

def test():
    uart = define(('Service', 1, 'test.uart'))
    uart.add(define(('Characteristic', 2, 'test.uart_write', 'utf8s')), 'data', CharacteristicProperties.Write)
    uart.add(define(('Characteristic', 3, 'test.uart_notify', 'utf8s')), 'data', CharacteristicProperties.Notify)
    
    wechat = define(('Service', 0xfee7, 'test.wechat'))
    wechat.add(define(('Characteristic', 0xfec7, 'test.wechat_write', 'utf8s')), 'data', CharacteristicProperties.Write)
    wechat.add(define(('Characteristic', 0xfec8, 'test.wechat_notify', 'utf8s')), 'data', CharacteristicProperties.Notify)
    wechat.add(define(('Characteristic', 0xfec9, 'test.wechat_read', 'utf8s')), 'data', CharacteristicProperties.Read)
    
    
    Profile(sys.stdout,
        GenericAccess('SYD BLE Mouse', 'Mouse', PeripheralPreferredConnectionParameters(6, 6, 48, 100)),
        GenericAttribute( (0, 0) ),
        DeviceInformation('SYD', 'BLE-MODEL-001', 'BLE-001', '1.0', '1.0', '1.0', PnpId(2, 0x93a, 0x4254, 1)),
        BatteryService(100),
        Uart('\x00'*16, '\x00'*16),
        Wechat('\x00'*16, '\x00'*16, '\x00'*16),
    )


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print '''Usage:
  Show all Services
    bleprofile.py -s
  Show all Characteristics
    bleprofile.py -c
  Show all Descriptors
    bleprofile.py -d
  Search
    bleprofile.py keyword
  Display test ble_service
    bleprofile.py -t'''
        sys.exit(0)
        
    globals()['BatteryService.BatteryLevel.Notify'] = 1
    init()
    if sys.argv[1][0] != '-':
        for v in Define.cache.values():
            if sys.argv[1] in v.name:
                print v
    
    elif '-t' in sys.argv:
        test()


    
