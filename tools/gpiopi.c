#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <stdbool.h>

#include <sys/mman.h>
#include <fcntl.h>

#define BCM2835_PERI_BASE   0x3f000000
#define BCM2835_GPIO_BASE	(BCM2835_PERI_BASE + 0x200000) /* GPIO controller */

#define BCM2835_PADS_GPIO_0_27		(BCM2835_PERI_BASE + 0x100000)
#define BCM2835_PADS_GPIO_0_27_OFFSET	(0x2c / 4)

/* GPIO setup macros */
#define MODE_GPIO(g) (*(pio_base+((g)/10))>>(((g)%10)*3) & 7)
#define INP_GPIO(g) do { *(pio_base+((g)/10)) &= ~(7<<(((g)%10)*3)); } while (0)
#define SET_MODE_GPIO(g, m) do { /* clear the mode bits first, then set as necessary */ \
		INP_GPIO(g);						\
		*(pio_base+((g)/10)) |=  ((m)<<(((g)%10)*3)); } while (0)
#define OUT_GPIO(g) SET_MODE_GPIO(g, 1)

#define GPIO_SET (*(pio_base+7))  /* sets   bits which are 1, ignores bits which are 0 */
#define GPIO_CLR (*(pio_base+10)) /* clears bits which are 1, ignores bits which are 0 */
#define GPIO_LEV (*(pio_base+13)) /* current level of the pin */

static int dev_mem_fd;
static volatile uint32_t *pio_base;

bool gpio_read(uint8_t idx)
{
    return (GPIO_LEV & (1<<idx)) > 0;
}

void gpio_set(uint8_t idx)
{
    GPIO_SET = 1<<idx;
}

void gpio_clr(uint8_t idx)
{
    GPIO_CLR = 1<<idx;
}

void gpio_mode(uint8_t idx, uint32_t mode)
{
    SET_MODE_GPIO(idx, mode);
}


bool gpio_init()
{
	dev_mem_fd = open("/dev/mem", O_RDWR | O_SYNC);
	if (dev_mem_fd < 0) {
		perror("open");
		return false;
	}

	pio_base = mmap(NULL, sysconf(_SC_PAGE_SIZE), PROT_READ | PROT_WRITE,
				MAP_SHARED, dev_mem_fd, BCM2835_GPIO_BASE);

	if (pio_base == MAP_FAILED) {
		perror("mmap");
		close(dev_mem_fd);
		return false;
	}

	static volatile uint32_t *pads_base;
	pads_base = mmap(NULL, sysconf(_SC_PAGE_SIZE), PROT_READ | PROT_WRITE,
				MAP_SHARED, dev_mem_fd, BCM2835_PADS_GPIO_0_27);

	if (pads_base == MAP_FAILED) {
		perror("mmap");
		close(dev_mem_fd);
		return false;
	}

	/* set 4mA drive strength, slew rate limited, hysteresis on */
	pads_base[BCM2835_PADS_GPIO_0_27_OFFSET] = 0x5a000008 + 1;
    return true;
}


static uint32_t swd_clk, swd_dio, swd_ndio;

#define swd_delay  GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0; GPIO_SET = 0;

#define swd_wcycle(b) { \
    GPIO_SET = swd_clk; \
    swd_delay \
    GPIO_CLR = swd_clk;  \
    if(b) GPIO_SET = swd_dio; else GPIO_CLR = swd_dio; \
    swd_delay \
}

#define swd_rcycle(v, n) { \
    GPIO_SET = swd_clk;  \
    swd_delay \
    v |= (GPIO_LEV & swd_dio) ? (n) : 0; \
    GPIO_CLR = swd_clk;  \
    swd_delay \
}

bool swd_init(uint8_t clk, uint8_t dio)
{
    if(!pio_base)gpio_init();
    if(!pio_base)return false;
    
    swd_clk = 1<<clk;
    swd_dio = 1<<dio;
    swd_ndio = dio;
    
    gpio_mode(clk, 1); // output
    gpio_mode(dio, 1); // output
    
    GPIO_SET = swd_clk | swd_dio;

    for(int i=0; i<56; i++) swd_wcycle(1);
    for(int i=0; i<16; i++) swd_wcycle(0xE79E & (1<<i));
    for(int i=0; i<56; i++) swd_wcycle(1);
    GPIO_SET = swd_clk; // trail
    return true;
}

void swd_exit(uint8_t clk, uint8_t dio)
{
    // GPIO 2,3 has pull-up resistor
    if(clk == 0 || clk >= 30){
        //...
    } else if(clk <= 3){
        gpio_mode(clk, 1); // output
        gpio_clr(clk); 
    } else {
        gpio_mode(clk, 0); // input
    }

    if(clk == 0 || clk >= 30){
        //...
    } else if(dio <= 3){
        gpio_mode(dio, 1); // output
        gpio_clr(dio); 
    } else {
        gpio_mode(dio, 0); // input
    }
    
    swd_clk = 0;
}

static uint8_t swd_header(uint16_t cfg)
{
    uint8_t v = 0;
    
    gpio_mode(swd_ndio, 1); // output
    
    
    // start
    swd_wcycle(1);
    
    // 0 DP, 1 AP
    if(cfg&0x100) v ^= 1; 
    swd_wcycle(cfg&0x100);
    
    // 0 W, 1 R
    if(cfg&0x200) v ^= 1;
    swd_wcycle(cfg&0x200);
    
    // A2
    if(cfg&4) v ^= 1;
    swd_wcycle(cfg&4);

    // A3
    if(cfg&8) v ^= 1;
    swd_wcycle(cfg&8);
    
    // P
    swd_wcycle(v);
    
    // STOP
    swd_wcycle(0);
    
    // PACK
    swd_wcycle(1);
    
    gpio_mode(swd_ndio, 0); // input
    
    // DUMMY
    swd_rcycle(v, 0);
    
    uint8_t ack = 0;
    
    // ACK0
    swd_rcycle(ack, 1);
    
    // ACK1
    swd_rcycle(ack, 2);
    
    // ACK2
    swd_rcycle(ack, 4);
    return ack;
}

void swd_idle()
{
    if(!swd_clk) return;
    gpio_mode(swd_ndio, 1);
    for(int i=0; i<10; i++)swd_wcycle(0);
    GPIO_SET = swd_clk; // trail
}

uint8_t swd_read(uint16_t cfg, uint32_t* dat)
{
    if(!swd_clk) return 0;
    
    uint8_t ack;
    for(int c=0; c<3; c++){
        uint8_t p = 0;
        uint8_t v = 0;
        ack = swd_header(cfg | 0x200);
        for(int i=0; i<32; i++){
            swd_rcycle(*dat, 1<<i);
            if(*dat & (1<<i)) p ^= 1;
        }
        swd_rcycle(v, 1);
        if(ack == 1 && v != p) ack = 0x80;
        gpio_mode(swd_ndio, 1);
        swd_wcycle(0); // DUMMY
        if(ack == 1 || ack == 4) break;
    }
    GPIO_SET = swd_clk; // trail
    return ack;
}

uint8_t swd_write(uint16_t cfg, uint32_t dat)
{
    if(!swd_clk) return 0;
    
    uint8_t ack = swd_header(cfg);
    
    gpio_mode(swd_ndio, 1);
    swd_wcycle(0); // DUMMY
    
    if(ack == 1){
        uint8_t p = 0;
        for(int i=0; i<32; i++){
            swd_wcycle( dat & (1<<i) );
            if(dat & (1<<i)) p ^= 1;
        }
        swd_wcycle(p);
    }
    GPIO_SET = swd_clk; // trail
    return ack;
}



