## 编译过程

### 操作系统 Ubuntu 18.04

### 安装gcc
```bash
cd /tmp
sudo apt install gcc-arm-none-eabi
wget http://ftp.us.debian.org/debian/pool/main/n/newlib/libnewlib-arm-none-eabi_3.0.0.20180831-1_all.deb
wget http://ftp.us.debian.org/debian/pool/main/n/newlib/libnewlib-dev_3.0.0.20180831-1_all.deb
sudo dpkg -i libnewlib-dev_3.0.0.20180831-1_all.deb
sudo dpkg -i libnewlib-arm-none-eabi_3.0.0.20180831-1_all.deb
```
    
### 下载代码和编译 syd8821.lib
```bash
git clone https://bitbucket.org/ham2me/syd8821.git
cd syd8821
make
```
    
### 编译和烧录 demo
```bash
cd demo
make T=name runbl
```
name需要被替换成 tled ble 等，用于不同的demo
如果不需要在烧录后进入miniterm模式，把 runbl 替换成 flashbl



## MIT LICENSE
Copyright (C) 2018    Wenjun Wang(wwj@ham2.me)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

